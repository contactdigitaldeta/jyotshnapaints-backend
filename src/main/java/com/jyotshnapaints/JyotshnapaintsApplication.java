package com.jyotshnapaints;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication (exclude = { SecurityAutoConfiguration.class })
public class JyotshnapaintsApplication {
	private static final Logger logger = LoggerFactory.getLogger(JyotshnapaintsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(JyotshnapaintsApplication.class, args);
			
		logger.error("message loged at ERROR level");
		logger.warn("message loged at WARN level");
		logger.info("message loged at INFO level");
		logger.debug("message loged at DEBUG level");
		
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*")
				.allowedMethods("GET","POST","PUT", "DELETE");
			}
		};
	}

}
