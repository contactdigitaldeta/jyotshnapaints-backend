package com.jyotshnapaints.common;

public enum LEADTYPE {
	
	WALKING_LEAD(1), 
	REF_LEAD(2),
	CC_LEAD(3),
	SOC_MED_LEAD(4),
	SALES_LEAD(5);
	
	private int value;

	LEADTYPE(int value) {
		this.value = value;
	}
	
	public int getLeadCode() {
		return this.value;
	}
}
