package com.jyotshnapaints.controller;

import com.jyotshnapaints.dto.CustomerDTO;
import com.jyotshnapaints.dto.LeadDTO;
import com.jyotshnapaints.entity.Lead;
import com.jyotshnapaints.entity.ResponseType;
import com.jyotshnapaints.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


public abstract class ALeadController implements ILeadController {
	
	@Autowired
	private LeadService walkingleadService;
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<Lead> getWalkingLeadById(@PathVariable("id") Long id) {

		return walkingleadService.getWalkingLeadById(id);
	}

	@PutMapping("/{id}")
	public String updateWalkingLead(@RequestBody LeadDTO walkingLead, @PathVariable("id") Long id) {
		System.out.println("id is " + id);
		// This method will update the details of an existing customer
		return walkingleadService.updateWalkingLead(walkingLead, id);
	}

	@PostMapping(consumes = "application/json")
	public ResponseEntity<ResponseType> createWalkingLead(@RequestBody LeadDTO walkingLead) {
		String response = walkingleadService.insertWalkingLead(walkingLead);		
		ResponseType responseType = new ResponseType();
		responseType.setMessage(response);
		responseType.setCodeValue(HttpStatus.CREATED.toString());
		ResponseEntity<ResponseType> responseEntity = new ResponseEntity<>(responseType, HttpStatus.CREATED);
		return responseEntity;
	}

	@DeleteMapping("/{id}")
	public String deleteWalkingLead(@PathVariable("id") Long id) {
		// This method will delete a user
		return walkingleadService.deleteWalkingLead(id);
	}
	
	@PostMapping("mapcustomer/{id}")
	public String mapLeadToCustomer(@RequestBody CustomerDTO cutomerDTO, @PathVariable("id") Long id) {	
		return walkingleadService.mapLeadToCustomer(id, cutomerDTO);
	}
}
