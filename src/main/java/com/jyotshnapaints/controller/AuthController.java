package com.jyotshnapaints.controller;

import java.util.HashSet;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jyotshnapaints.entity.ERole;
import com.jyotshnapaints.entity.LoginUser;
import com.jyotshnapaints.entity.Role;
import com.jyotshnapaints.jwt.JwtUtils;
import com.jyotshnapaints.repository.LoginUserRepository;
import com.jyotshnapaints.repository.RoleRepository;
import com.jyotshnapaints.request.LoginRequest;
import com.jyotshnapaints.request.SignupRequest;
import com.jyotshnapaints.response.JwtResponse;
import com.jyotshnapaints.response.MessageResponse;
import com.jyotshnapaints.service.UserDetailsImpl;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	LoginUserRepository loginUserRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	

	@PostMapping("/signin")
	public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}
	
	@PostMapping("/signup")
	public ResponseEntity<MessageResponse> registerUser(@RequestBody SignupRequest signUpRequest) {
		if (Boolean.TRUE.equals(loginUserRepository.existsByUsername(signUpRequest.getUsername()))) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (Boolean.TRUE.equals(loginUserRepository.existsByEmail(signUpRequest.getEmail()))) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		LoginUser user = new LoginUser(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		List<String> strRoles = signUpRequest.getRoles();
		Set<Role> roles = new HashSet<>();
	
			strRoles.forEach(role -> {
				String err = "Error: Role is not found.";
				switch (role) {
				case "teleCaller":
					Role teleCallerRole = roleRepository.findByRoleName(ERole.ROLE_TELECALLER)
							.orElseThrow(() -> new RuntimeException(err));
					roles.add(teleCallerRole);

					break;
					
				case "admin":
					Role adminRole = roleRepository.findByRoleName(ERole.ROLE_ADMIN)
					.orElseThrow(() -> new RuntimeException(err));
			roles.add(adminRole);
					
					break;
					
				case "marketingAgent":
					Role marketingAgentRole = roleRepository.findByRoleName(ERole.ROLE_MARKETINGAGENT)
					.orElseThrow(() -> new RuntimeException(err));
			roles.add(marketingAgentRole);
					
					break;
					
				default :
					
					// Create a Logger
			        Logger logger
			            = Logger.getLogger(
			            		AuthController.class.getName());
					logger.log(Level.WARNING,"Role not present");					
				}
			});
			
		
		user.setRoles(roles);
		loginUserRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}	
	

}
