package com.jyotshnapaints.controller;

import com.jyotshnapaints.service.LeadService;
import com.jyotshnapaints.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private LeadService leadService;

    @Autowired
    private ProjectService projectService;

    @GetMapping(value = "/allCount", produces = "application/json")
    public Map<String, Long> getAllCont() {
        Map<String, Long> allLeadCount = leadService.getAllLeadCount();
        Long projectCount = projectService.getProjectCount();
        allLeadCount.put("projectCount", projectCount);
        return allLeadCount;
    }

    @GetMapping(value = "lead/allAssignTo", produces = "application/json")
    public Set<String> getAllAssignTo() {
        List<String> listOfAssignTo = leadService.getAllLeadAssignTo();
        listOfAssignTo.sort(Comparator.naturalOrder());
        Set<String> uniqueVals = removeDuplicate(listOfAssignTo);
        return uniqueVals;
    }

    private Set<String> removeDuplicate(List<String> values) {
        Set<String> s = new HashSet<>();
        values.forEach(v-> s.add(v));
        return s;
    }

}
