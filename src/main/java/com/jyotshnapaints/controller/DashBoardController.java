package com.jyotshnapaints.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jyotshnapaints.dto.DashBoardDTO;
import com.jyotshnapaints.entity.Lead;
import com.jyotshnapaints.service.DashBoardService;

@RestController
@RequestMapping("/dashBoard")
public class DashBoardController {
	
	@Autowired
	private DashBoardService dashBoardService;
	
	@PostMapping(produces = "application/json")
	public List<Lead> getDashBoardDetails(@RequestBody DashBoardDTO dashBoard) {
		return dashBoardService.getDashBoardDetails(dashBoard);
	}

}
