package com.jyotshnapaints.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.jyotshnapaints.dto.LeadDTO;
import com.jyotshnapaints.entity.Lead;
import com.jyotshnapaints.entity.ResponseType;

public interface ILeadController {
	
	public List<Lead> getLead();
	
	public Optional<Lead> getWalkingLeadById(Long id);
	
	public String updateWalkingLead(LeadDTO walkingLead, Long id);
	
	public ResponseEntity<ResponseType> createWalkingLead(LeadDTO walkingLead);
	
	public String deleteWalkingLead(Long id);

}
