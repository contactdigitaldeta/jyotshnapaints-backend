package com.jyotshnapaints.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jyotshnapaints.dto.ProjectDTO;
import com.jyotshnapaints.entity.Project;
import com.jyotshnapaints.service.ProjectService;

@RestController
@RequestMapping("/projects")
//@CrossOrigin(origins="*")
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@GetMapping(produces="application/json")
	public List<Project> getProjects() {
		
		return projectService.getProjects();
	}
	
	@GetMapping(value="/{id}", produces="application/json")
	public Optional<Project> getProjectsById(@PathVariable("id") Long id) {
		
		return projectService.getProjectById(id);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<String> createProject(@RequestBody ProjectDTO project) 
	{
		String response = projectService.insertProject(project);
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/{id}")
	public String updateProject(@RequestBody ProjectDTO project, @PathVariable("id") Long id)
	{	
		System.out.println("id is "+ id);
		//This method will update the details of an existing customer 
		return projectService.updateProject(project, id);
	}
	
	@DeleteMapping("/{id}")
	public String deleteProject(@PathVariable("id") Long id) 
	{
		//This method will delete a customer 
		return projectService.deleteProject(id);
	}
}
