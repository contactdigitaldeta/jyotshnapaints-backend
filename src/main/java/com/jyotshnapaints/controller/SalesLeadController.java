package com.jyotshnapaints.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jyotshnapaints.entity.Lead;
import com.jyotshnapaints.service.LeadService;

@RestController
@RequestMapping("/salelead")
//@CrossOrigin(origins="*")
public class SalesLeadController extends ALeadController{
	
	@Autowired
	private LeadService salesleadService;

	@GetMapping(produces = "application/json")
	public List<Lead> getLead() {
		return salesleadService.getSalesLead();
	}
	
}
