package com.jyotshnapaints.controller;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jyotshnapaints.dto.ProjectDTO;
import com.jyotshnapaints.dto.UserDTO;
import com.jyotshnapaints.entity.Project;
import com.jyotshnapaints.entity.User;
import com.jyotshnapaints.service.ProjectService;
import com.jyotshnapaints.service.UserService;


@SuppressWarnings("unused")
@RestController
@RequestMapping("/userlist")
//@CrossOrigin(origins="*")
public class UserController {
	
	
	@Autowired
	private UserService userService;
	
	@GetMapping(produces="application/json")
	public List<User> getUsers() {
		
			return userService.getUsers();
	}
	
	@GetMapping(value="/{id}", produces="application/json")
	public Optional<User> getUsertById(@PathVariable("id") Long id) {
		
		return userService.getUsertById(id);
	}
	
	
	@PutMapping("/{id}")
	public String updateUser(@RequestBody UserDTO user, @PathVariable("id") Long id)
	{	
		System.out.println("id is "+ id);
		//This method will update the details of an existing customer 
		return userService.updateUser(user, id);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<String> createUser(@RequestBody UserDTO user) 
	{
		String response = userService.insertUser(user);
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	public String deleteUser(@PathVariable("id") Long id) 
	{
		//This method will delete a user
		return userService.deleteUser(id);
	}
	
	
	
	

}
