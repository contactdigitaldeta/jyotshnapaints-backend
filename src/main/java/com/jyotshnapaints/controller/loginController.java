package com.jyotshnapaints.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jyotshnapaints.entity.Login;
import com.jyotshnapaints.service.LoginService;

@RestController
@RequestMapping("/login")
public class loginController {
	
	@Autowired
	private LoginService loginService;
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<Boolean> login(@RequestBody Login login) 
	{
		Boolean response = loginService.checkLogin(login.getUserName(), login.getPassword());
		return ResponseEntity.ok(response);
	}
	
}
