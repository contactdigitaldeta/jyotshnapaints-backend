package com.jyotshnapaints.dto;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jyotshnapaints.entity.Customer;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class CustomerDTO {

	String name;
	Long phoneNumber;
	String emailId;
	String address;
	String city;
	String state;
	Integer pincode;
	String companyName;
	String designation;
	String sources;
	String assignTo;
	String comment;
	String status;
	String reference;
	@Temporal(TemporalType.DATE)
	Date uploadDate;

	public static Customer prepareCustomerEntity(CustomerDTO customerDTO) {
		return Customer.builder()
		 .name(customerDTO.getName())
		 .phoneNumber(customerDTO.getPhoneNumber())
		 .emailId(customerDTO.getEmailId())
		 .address(customerDTO.getAddress())
		 .city(customerDTO.getCity())
		 .state(customerDTO.getState())
		 .pincode(customerDTO.getPincode())
		 .companyName(customerDTO.getCompanyName())
		 .designation(customerDTO.getDesignation())
		 .sources(customerDTO.getSources())
		 .assignTo(customerDTO.getAssignTo())
		 .comment(customerDTO.getComment())
		 .status(customerDTO.getStatus())
		 .reference(customerDTO.getReference())
		 .creation_tt(Calendar.getInstance().getTime())
		 .updated_tt(Calendar.getInstance().getTime())
		 .uploadDate(customerDTO.getUploadDate())
		 .build();
	}
}
