package com.jyotshnapaints.dto;

import java.sql.Date;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DashBoardDTO {
	
	Date starDate;
	Date endDate;
	Integer leadType;
	String assignTo;
	Date recallDate;

	
}
