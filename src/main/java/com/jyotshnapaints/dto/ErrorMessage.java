package com.jyotshnapaints.dto;

import lombok.Data;

@Data
public class ErrorMessage {
	
	private int errorCode;
	private String message;

}
