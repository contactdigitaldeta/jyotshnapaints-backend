package com.jyotshnapaints.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jyotshnapaints.entity.Lead;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LeadDTO {
	
	String name;
	Long phoneNumber;
	String emailId;
	String address;
	String city;
	String state;
	Integer pincode;
	String companyName;
	String designation;
	String sources;
	String assignTo;
	String comment;
	String status;
	Integer leadType;
	Integer reference;
	String coldCallType;
	List<String> requirements;
	String jobWorkDetails;
	String prevRequirements;
	@Temporal(TemporalType.DATE)
	Date uploadDate;
	@Temporal(TemporalType.DATE)
	Date recallDate;
	String remarks;

	public static Lead prepareLeadEntity(LeadDTO leadDTO) {
		return Lead.builder()
		 .name(leadDTO.getName())
		 .phoneNumber(leadDTO.getPhoneNumber())
		 .emailId(leadDTO.getEmailId())
		 .address(leadDTO.getAddress())
		 .city(leadDTO.getCity())
		 .state(leadDTO.getState())
		 .pincode(leadDTO.getPincode())
		 .companyName(leadDTO.getCompanyName())
		 .designation(leadDTO.getDesignation())
		 .sources(leadDTO.getSources())
		 .assignTo(leadDTO.getAssignTo())
		 .comment(leadDTO.getComment())
		 .status(leadDTO.getStatus())
		 .requirements(leadDTO.getRequirements())
		 .leadType(leadDTO.getLeadType())
		 .reference(leadDTO.getReference())
		 .coldCallType(leadDTO.getColdCallType())
		 .creation_tt(Calendar.getInstance().getTime())
		 .updated_tt(Calendar.getInstance().getTime())
		 .prevRequirements(leadDTO.getPrevRequirements())
		 .jobWorkDetails(leadDTO.getJobWorkDetails())
		 .uploadDate(leadDTO.getUploadDate())
		 .recallDate(leadDTO.getRecallDate())
		 .remarks(leadDTO.getRemarks())
		 .build();
	}
	
}
