package com.jyotshnapaints.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.jyotshnapaints.entity.Project;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO {
	
	 String name;
	 String developer;
	 String constructionStatus;
	 String personName;
	 String designation;
	 Long mobileNo;
	 Long alternateMobile;
	 String emailId;
	 String address1;
	 String address2;
	 String locality;
	 String area;
	String city;
	String state;
	String totalPlotArea;
	String totalUnit;
	String assignTo;
	String sourceName;
	String require;
	String sideVisitedBy;
	String quotation;
	String workInProgress;
	String close;
	String quotationAmount;
	String remark;
	@Temporal(TemporalType.DATE)
	Date uploadDate;
	
	public static Project prepareProjectEntity(ProjectDTO projectDTO)
	{
		
		return Project.builder()
		.name(projectDTO.getName())
		.developer(projectDTO.getDeveloper())
		.constructionStatus(projectDTO.getConstructionStatus())
		.personName(projectDTO.getPersonName())
		.designation(projectDTO.getDesignation())
		.mobileNo(projectDTO.getMobileNo())
		.alternateMobile(projectDTO.getAlternateMobile())
		.emailId(projectDTO.getEmailId())
		.address1(projectDTO.getAddress1())
		.address2(projectDTO.getAddress2())
		.locality(projectDTO.getLocality())
		.area(projectDTO.getArea())
		.city(projectDTO.getCity())
		.state(projectDTO.getState())
		.totalPlotArea(projectDTO.getTotalPlotArea())
		.totalUnit(projectDTO.getTotalUnit())
		.assignTo(projectDTO.getAssignTo())
		.require(projectDTO.getRequire())
		.sideVisitedBy(projectDTO.getSideVisitedBy())
		.quotation(projectDTO.getQuotation())
		.workInProgress(projectDTO.getWorkInProgress())
		.close(projectDTO.getClose())
		.quotationAmount(projectDTO.getQuotationAmount())
		.remark(projectDTO.getRemark())
		.sourceName(projectDTO.getSourceName())
		.uploadDate(projectDTO.getUploadDate())
		.build();
	
	}

}
