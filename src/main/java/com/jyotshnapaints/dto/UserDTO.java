package com.jyotshnapaints.dto;

import com.jyotshnapaints.entity.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
	 
	//private String name;
	private String name;
	Long phonenumber;
	String emailid;
	String password;
	Long role;
	
	
	public static User prepareUserEntity(UserDTO user){
		
		 return User.builder()
					.name(user.getName())
					.phonenumber(user.getPhonenumber())
					.emailid(user.getEmailid())
					.password(user.getPassword()) 
					.role(user.getRole()).build();
	}
	
}
