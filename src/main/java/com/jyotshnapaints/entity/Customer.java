package com.jyotshnapaints.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



import com.jyotshnapaints.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customerId")
	Long Id;
	String name;
	Long phoneNumber;
	String emailId;
	String address;
	String alternateAddress;
	String city;
	String state;
	Integer pincode;
	String companyName;
	String designation;
	String sources;
	String assignTo;
	String comment;
	String status;
	String reference;
	@Temporal(TemporalType.DATE)
	Date creation_tt;
	@Temporal(TemporalType.DATE)
	Date updated_tt;
	@Temporal(TemporalType.DATE)
	Date uploadDate;
	

	public static CustomerDTO prepareCustomerDTO(Customer customer) {
		return CustomerDTO.builder().name(customer.getName()).phoneNumber(customer.getPhoneNumber()).emailId(customer.getEmailId())
				.address(customer.getAddress()).city(customer.getCity()).state(customer.getState()).pincode(customer.getPincode())
				.companyName(customer.getCompanyName()).designation(customer.getDesignation()).sources(customer.getSources())
				.assignTo(customer.getAddress()).comment(customer.getComment()).status(customer.getStatus())
				.reference(customer.getReference())
				.uploadDate(customer.getUploadDate())
				.build();
	}

}
