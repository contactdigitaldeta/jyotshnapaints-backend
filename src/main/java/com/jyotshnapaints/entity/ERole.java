package com.jyotshnapaints.entity;

public enum ERole {

	ROLE_ADMIN,
	ROLE_TELECALLER,
	ROLE_MARKETINGAGENT
}
