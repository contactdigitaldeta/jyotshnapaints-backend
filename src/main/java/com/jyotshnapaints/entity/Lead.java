package com.jyotshnapaints.entity;

import java.util.Date;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import com.jyotshnapaints.dto.LeadDTO;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@TypeDef(
	    name = "list-array",
	    typeClass = ListArrayType.class
	)
@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@NamedQuery(name = "Lead.findByAssignToAndCreationTTQuery",
//query = "SELECT l.lead_id,l.assign_to, l.creation_tt,l.lead_type,l.name, l.email_id, l.phone_number "
//		+ "FROM jyotshna_crm.lead l where l.lead_type=?1 and l.assign_to=?2 and l.creation_tt between ?3 and ?4")
public class Lead {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "leadId")
	Long Id;
	String name;
	Long phoneNumber;
	String emailId;
	String address;
	String city;
	String state;
	Integer pincode;
	String companyName;
	String designation;
	String sources;
	String assignTo;
	String comment;
	String status;
	Integer leadType;
	Integer reference;
	String coldCallType;
	String remarks;
	@Temporal(TemporalType.DATE)
	Date creation_tt;
	@Temporal(TemporalType.DATE)
	Date updated_tt;
	@Type(type = "list-array")
	@Column(name = "requirements", columnDefinition = "text[]")
	List<String> requirements;
	String jobWorkDetails;
	String prevRequirements;
	@Temporal(TemporalType.DATE)
	Date uploadDate;
	@Temporal(TemporalType.DATE)
	Date recallDate;

	public static LeadDTO prepareLeadDTO(Lead lead) {
		return LeadDTO.builder().name(lead.getName()).phoneNumber(lead.getPhoneNumber()).emailId(lead.getEmailId())
				.address(lead.getAddress()).city(lead.getCity()).state(lead.getState()).pincode(lead.getPincode())
				.companyName(lead.getCompanyName()).designation(lead.getDesignation()).sources(lead.getSources())
				.assignTo(lead.getAddress()).comment(lead.getComment()).status(lead.getStatus())
				.requirements(lead.getRequirements()).leadType(lead.getLeadType()).reference(lead.getReference())
				.jobWorkDetails(lead.getJobWorkDetails()).prevRequirements(lead.getPrevRequirements())
				.coldCallType(lead.getColdCallType()).recallDate(lead.getRecallDate()).uploadDate(lead.getUploadDate())
				.remarks(lead.getRemarks())
				.build();

	}

}
