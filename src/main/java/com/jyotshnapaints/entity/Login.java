package com.jyotshnapaints.entity;

import lombok.Data;

@Data
public class Login {
	
	private String userName;
	
	private String password;

}
