package com.jyotshnapaints.entity;

import java.util.Date;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jyotshnapaints.dto.ProjectDTO;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
public class Project {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "projectId")
	Long Id;
	String name;
	String developer;
	String constructionStatus;
	String personName;
	String designation;
	Long   mobileNo;
	Long   alternateMobile;
	String emailId;
	String address1;
	String address2;
	String locality;
	String area;
	String city;
	String state;
	String totalPlotArea;  
	String totalUnit;
	String assignTo;
	String sourceName;
	String require;
	String sideVisitedBy;
	String quotation;
	String workInProgress;
	String close;
	String quotationAmount;
	String remark;
	@Temporal(TemporalType.DATE)
	Date uploadDate;
	
	
	public static ProjectDTO prepareProjectDTO(Project project)
	{
		return ProjectDTO.builder()
				.name(project.getName())
				.developer(project.getDeveloper())
				.constructionStatus(project.getConstructionStatus())
				.personName(project.getPersonName())
				.designation(project.getDesignation())
				.mobileNo(project.getMobileNo())
				.alternateMobile(project.getAlternateMobile())
				.emailId(project.getEmailId())
				.address1(project.getAddress1())
				.address2(project.getAddress2())
				.locality(project.getLocality())
				.area(project.getArea())
				.city(project.getCity())
				.state(project.getState())
				.totalPlotArea(project.getTotalPlotArea())
				.totalUnit(project.getTotalUnit())
				.assignTo(project.getAssignTo())
				.require(project.getRequire())
				.sideVisitedBy(project.getSideVisitedBy())
				.quotation(project.getQuotation())
				.workInProgress(project.getWorkInProgress())
				.close(project.getClose())
				.quotationAmount(project.getQuotationAmount())
				.remark(project.getRemark())
				.sourceName(project.getSourceName())
				.uploadDate(project.getUploadDate())
				.build();
		
	}





	public Project(Long id, String name, String developer, String constructionStatus, String personName,
			String designation, Long mobileNo, Long alternateMobile, String emailId, String address1, String address2,
			String locality, String area, String city, String state, String totalPlotArea, String totalUnit,
			String assignTo, String sourceName,String require,String sideVisitedBy,String quotation,String workInProgress
			,String close,String quotationAmount,String remark,Date uploadDate) {
		super();
		Id = id;
		this.name = name;
		this.developer = developer;
		this.constructionStatus = constructionStatus;
		this.personName = personName;
		this.designation = designation;
		this.mobileNo = mobileNo;
		this.alternateMobile = alternateMobile;
		this.emailId = emailId;
		this.address1 = address1;
		this.address2 = address2;
		this.locality = locality;
		this.area = area;
		this.city = city;
		this.state = state;
		this.totalPlotArea = totalPlotArea;
		this.totalUnit = totalUnit;
		this.assignTo = assignTo;
		this.sourceName = sourceName;
		this.require = require;
		this.sideVisitedBy= sideVisitedBy;
		this.quotation = quotation;
		this.workInProgress = workInProgress;
		this.close = close;
		this.quotationAmount = quotationAmount;
		this.remark = remark;
		this.uploadDate=uploadDate;
	}
	
}
