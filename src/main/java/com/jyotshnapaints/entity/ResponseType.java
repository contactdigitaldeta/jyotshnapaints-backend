package com.jyotshnapaints.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseType {
	
	@JsonProperty("message")
	String message;
	
	@JsonProperty("codeValue")
	String codeValue;

}
