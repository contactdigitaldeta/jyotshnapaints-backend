package com.jyotshnapaints.entity;


import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jyotshnapaints.dto.UserDTO;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@ToString
@Builder
@NoArgsConstructor
public class User {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userId")
	Long Id;
	String name;
	Long phonenumber;
	String emailid;
	String password;
	Long role;
	
	
	
	public static UserDTO prepareUserDTO(User user) {
		
		   return UserDTO.builder()
				.name(user.getName())
				.phonenumber(user.getPhonenumber())
				.emailid(user.getEmailid())
				.password(user.getPassword())
				.role(user.getRole()).build();
				
	}
	
    public User(Long id,String name,Long phonenumber,String emailid,String password,Long role) {
    	
    	super();
		Id = id;
    	this.name = name;
    	this.phonenumber=phonenumber;
    	this.emailid=emailid;
    	this.password=password;
    	this.role=role;
    	
    	
    }
	
	
	
	
}
