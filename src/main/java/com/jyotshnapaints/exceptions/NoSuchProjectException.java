package com.jyotshnapaints.exceptions;

public class NoSuchProjectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoSuchProjectException() {
		super();
	}
	
	public NoSuchProjectException(String errors) {
		super(errors);
	}


}
