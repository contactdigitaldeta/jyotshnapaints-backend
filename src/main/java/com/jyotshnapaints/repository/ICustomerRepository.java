package com.jyotshnapaints.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jyotshnapaints.entity.Customer;


public interface ICustomerRepository extends JpaRepository<Customer, Long>{

}
