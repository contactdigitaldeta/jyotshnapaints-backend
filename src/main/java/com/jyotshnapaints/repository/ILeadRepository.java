package com.jyotshnapaints.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jyotshnapaints.entity.Lead;


public interface ILeadRepository extends JpaRepository<Lead, Long> {
	
	static String query = "SELECT * "
			+ "FROM jyotshna_crm.lead l where l.lead_type=?1 and l.assign_to=?2 and l.creation_tt between ?3 and ?4";

	static String FIND_LEAD_ASSIGN_TO = "select assign_to from jyotshna_crm.lead where assign_to notnull and assign_to!=''";
	
	static String FIND_BY_DATE_ONLY = "SELECT * "
			+ "FROM jyotshna_crm.lead l where l.creation_tt between ?1 and ?2";
	
	static String Find_BY_ASSIGNTO_AND_DATE = "SELECT * "
			+ "FROM jyotshna_crm.lead l where l.assign_to=?1 and l.creation_tt between ?2 and ?3";
	
	static String FIND_BY_RECALLDATE_AND_SOURCENAME = "";
	
	List<Lead> findByLeadType(Integer leadType);
	long countByLeadType(Integer leadType);

	
	@Query(value=query, nativeQuery=true)
	List<Lead> findByAssignToAndCreationTTQuery(Integer type, String assignTo, Date StartDate, Date endDate);

	@Query(value=FIND_LEAD_ASSIGN_TO, nativeQuery=true)
	List<String> findAllAssignTo();
	
	@Query(value =FIND_BY_DATE_ONLY ,nativeQuery = true)
	List<Lead> findLeadByDate(Date StartDate, Date endDate);
	
	@Query(value=Find_BY_ASSIGNTO_AND_DATE , nativeQuery= true)
	List<Lead>findbyassigntoanddate(String assignTO, Date StartDate, Date endDate );
	
	
	List<Lead> findByRecallDate(Date recallDate);
	
	
	
	
	
	
	
	
		
}
