package com.jyotshnapaints.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jyotshnapaints.entity.Project;

public interface IProjectRepository extends JpaRepository<Project, Long>{
	
	

	
}
