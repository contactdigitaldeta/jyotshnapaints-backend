package com.jyotshnapaints.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.jyotshnapaints.entity.User;


public interface IUserRepository extends JpaRepository<User, Long> {

	List<User> findByName(String username);
	
}
