package com.jyotshnapaints.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jyotshnapaints.dto.CustomerDTO;
import com.jyotshnapaints.entity.Customer;
import com.jyotshnapaints.repository.ICustomerRepository;




@Service
public class CustomerService {
	

	@Autowired
	private ICustomerRepository customerRepository;
	
	public List<Customer> getCustomers() {
		return customerRepository.findAll();
	}
	
	public String insertCustomer(CustomerDTO customer) {
		customerRepository.saveAndFlush(CustomerDTO.prepareCustomerEntity(customer));
		return "Customer Added successfully";
	}

	public Optional<Customer> getCustomerById(Long id) {
		return customerRepository.findById(id);
	}
	
	public String updateCustomer(CustomerDTO customer, Long id) {
		Optional<Customer> optionalCustomer = customerRepository.findById(id);
		Customer customerEntity = optionalCustomer.get();
		

		customerEntity.setName(customer.getName());
		customerEntity.setPhoneNumber(customer.getPhoneNumber());
		customerEntity.setEmailId(customer.getEmailId());
		customerEntity.setAddress(customer.getAddress());
		customerEntity.setCity(customer.getCity());
		customerEntity.setState(customer.getState());
		customerEntity.setPincode(customer.getPincode());
		customerEntity.setCompanyName(customer.getCompanyName());
		customerEntity.setDesignation(customer.getDesignation());
		customerEntity.setSources(customer.getSources());
		customerEntity.setAssignTo(customer.getAssignTo());
		customerEntity.setComment(customer.getComment());
		customerEntity.setStatus(customer.getStatus());
		customerEntity.setReference(customer.getReference());
		customerEntity.setUploadDate(customer.getUploadDate());
		customerRepository.saveAndFlush(customerEntity);
		return "Customer updated successfully.";
		
		}
		public String deleteCustomer(Long id) {
			Optional<Customer> optionalCustomer = customerRepository.findById(id);
			if(optionalCustomer != null) {
				customerRepository.deleteById(id);
			}		
			return "Customer Deleted SuccessFully"+ optionalCustomer.get().getName()+", "+  optionalCustomer.get().getId();
		}  
	
		public Long getCustomerCount() {
			return customerRepository.count();
		}
	
}
