package com.jyotshnapaints.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jyotshnapaints.dto.DashBoardDTO;
import com.jyotshnapaints.entity.Lead;
import com.jyotshnapaints.repository.ILeadRepository;

@Service
public class DashBoardService {
	
	@Autowired
	private ILeadRepository leadRepository;
	
	public List<Lead> getDashBoardDetails(DashBoardDTO dashBoardDetails) {
		
//		DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");
//		DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		String assignTo = dashBoardDetails.getAssignTo();
		Integer leadType = dashBoardDetails.getLeadType();

		Date recallDate = dashBoardDetails.getRecallDate();
		Date startdate = dashBoardDetails.getStarDate();
		Date endDate = dashBoardDetails.getEndDate();
		
		List<Lead> listoffilterdata = new ArrayList<Lead>();
				
		if(assignTo !=null && leadType !=null && startdate !=null && endDate !=null) {
			listoffilterdata = leadRepository.findByAssignToAndCreationTTQuery(leadType, assignTo, startdate, endDate);
		}
		
		else if(assignTo !=null && leadType ==null && startdate !=null && endDate !=null) {
			listoffilterdata = leadRepository.findbyassigntoanddate(assignTo, startdate, endDate);
		}
		
		else if(assignTo ==null && leadType ==null && startdate !=null && endDate !=null) {
			listoffilterdata =  leadRepository.findLeadByDate(startdate, endDate);
		}
		else if (recallDate !=null && assignTo ==null && leadType ==null && startdate==null && endDate == null) {
			listoffilterdata = leadRepository.findByRecallDate(recallDate);
		}
		
		return listoffilterdata;
		
		
		
		
		
		
		
		
		
	}

}
