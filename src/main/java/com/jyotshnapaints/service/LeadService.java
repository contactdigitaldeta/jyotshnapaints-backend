package com.jyotshnapaints.service;

import java.util.HashMap;
import java.util.List;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jyotshnapaints.dto.CustomerDTO;
import com.jyotshnapaints.dto.LeadDTO;
import com.jyotshnapaints.entity.Customer;
import com.jyotshnapaints.entity.Lead;
import com.jyotshnapaints.repository.ICustomerRepository;
import com.jyotshnapaints.repository.ILeadRepository;

@Service
public class LeadService {
	
	@Autowired
	private ILeadRepository leadRepository;
	
	@Autowired
	private ICustomerRepository cutomerRepository;
	
	public static final Integer WALKING_LEAD = 1;
	public static final Integer REF_LEAD = 2;
	public static final Integer CC_LEAD = 3;
	public static final Integer SOC_MED_LEAD = 4;
	public static final Integer SALES_LEAD = 5;
	
	
	
	@Cacheable(value="Lead-cache",key="'LeadCache'")
	public List<Lead> getWalkingLead() {
		return leadRepository.findByLeadType(WALKING_LEAD);
	}
	 
	public List<Lead> getReferenceLead() {
		return leadRepository.findByLeadType(REF_LEAD);
	}
	
	public List<Lead> getColdCallLead() {
		return leadRepository.findByLeadType(CC_LEAD);
	}
	
	public List<Lead> getSocialMedLead() {
		return leadRepository.findByLeadType(SOC_MED_LEAD);
	}
	
	public List<Lead> getSalesLead() {
		return leadRepository.findByLeadType(SALES_LEAD);
	}

	public Optional<Lead> getWalkingLeadById(Long id) {
		return leadRepository.findById(id);
	}

	public String updateWalkingLead(LeadDTO lead, Long id) {
		Optional<Lead> OptionalLead = leadRepository.findById(id);
		Lead leadEntity = OptionalLead.get();
		
		leadEntity.setName(lead.getName());
		leadEntity.setPhoneNumber(lead.getPhoneNumber());
		leadEntity.setEmailId(lead.getEmailId());
		leadEntity.setAddress(lead.getAddress());
		leadEntity.setCity(lead.getCity());
		leadEntity.setState(lead.getState());
		leadEntity.setPincode(lead.getPincode());
		leadEntity.setCompanyName(lead.getCompanyName());
		leadEntity.setDesignation(lead.getDesignation());
		leadEntity.setSources(lead.getSources());
		leadEntity.setAssignTo(lead.getAssignTo());
		leadEntity.setComment(lead.getComment());
		leadEntity.setStatus(lead.getStatus());
		leadEntity.setLeadType(lead.getLeadType());
		leadEntity.setReference(lead.getReference());
		leadEntity.setColdCallType(lead.getColdCallType());
		leadEntity.setRequirements(lead.getRequirements());
		leadEntity.setUploadDate(lead.getUploadDate());
		leadEntity.setRecallDate(lead.getRecallDate());
		leadRepository.saveAndFlush(leadEntity);
		return "Lead Updated Successfully.";
	}

	public String insertWalkingLead(LeadDTO walkingLead) {
		leadRepository.saveAndFlush(LeadDTO.prepareLeadEntity(walkingLead));
		return "Lead Added successfully";
	}

	public String deleteWalkingLead(Long id) {
		Optional<Lead> optionalLead = leadRepository.findById(id);
		if(optionalLead != null) {
			leadRepository.deleteById(id);
		}
		return "Lead Deleted SuccessFully"+ optionalLead.get().getName()+", "+  optionalLead.get().getId();
	}

	public Map<String, Long> getAllLeadCount() {
		long walkLeadCount = leadRepository.countByLeadType(WALKING_LEAD);
		long refLeadCount = leadRepository.countByLeadType(REF_LEAD);
		long ccLeadCount = leadRepository.countByLeadType(CC_LEAD);
		long socMedLeadCount = leadRepository.countByLeadType(SOC_MED_LEAD);
		long salesLeadCount = leadRepository.countByLeadType(SALES_LEAD);

		Map<String, Long> leadCountMap = new HashMap<>();
		leadCountMap.put("walkingLeadCount", walkLeadCount);
		leadCountMap.put("refLeadCount", refLeadCount);
		leadCountMap.put("ccLeadCount", ccLeadCount);
		leadCountMap.put("socLeadCount", socMedLeadCount);
		leadCountMap.put("salesLeadCount", salesLeadCount);
		return leadCountMap;
	}

	public List<String> getAllLeadAssignTo() {
		return leadRepository.findAllAssignTo();
	}
	
	public String mapLeadToCustomer(Long id, CustomerDTO customer) {
		String returnVal = "";
		Customer cust = cutomerRepository.saveAndFlush(CustomerDTO.prepareCustomerEntity(customer));
		if(cust != null) {
			returnVal = returnVal+"Customer is created successfully!!";
		} else {
			return "customer not created!!!";
		}
		deleteWalkingLead(id);				
		return returnVal;
	}

				
	}
