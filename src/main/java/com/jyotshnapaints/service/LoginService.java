package com.jyotshnapaints.service;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jyotshnapaints.entity.User;
import com.jyotshnapaints.repository.IUserRepository;

@Service
public class LoginService {
	
	@Autowired
	private IUserRepository userRepository;
	
	public boolean checkLogin(String username, String password) {
		
		boolean isPasswordMatch = false;
		
		List<User> userList = userRepository.findByName(username);
		if(userList.size() == 1) {
			isPasswordMatch = password == userList.get(0).getPassword();
		}
		return isPasswordMatch;		
	}
	

}
