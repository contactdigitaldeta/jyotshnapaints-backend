package com.jyotshnapaints.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jyotshnapaints.dto.ProjectDTO;
import com.jyotshnapaints.entity.Project;
import com.jyotshnapaints.repository.IProjectRepository;

@Service
public class ProjectService {
	
	@Autowired
	private IProjectRepository projectRepository;
	
	public List<Project> getProjects() {
		return projectRepository.findAll();
	}
	
	public String insertProject(ProjectDTO project) {
		projectRepository.saveAndFlush(ProjectDTO.prepareProjectEntity(project));
		return "Project Added successfully";
	}
	
	public Optional<Project> getProjectById(Long id) {
		return projectRepository.findById(id);
	}
	
	public String updateProject(ProjectDTO project, Long id) {
		Optional<Project> optionalProject = projectRepository.findById(id);
		Project projectEntity = optionalProject.get();
		
		projectEntity.setName(project.getName());
		projectEntity.setDeveloper(project.getDeveloper());
		projectEntity.setConstructionStatus(project.getConstructionStatus());
		projectEntity.setPersonName(project.getPersonName());
		projectEntity.setDesignation(project.getDesignation());
		projectEntity.setMobileNo(project.getMobileNo());
		projectEntity.setAlternateMobile(project.getAlternateMobile());
		projectEntity.setEmailId(project.getEmailId());
		projectEntity.setAddress1(project.getAddress1());
		projectEntity.setAddress2(project.getAddress2());
		projectEntity.setLocality(project.getLocality());
		projectEntity.setArea(project.getArea());
		projectEntity.setCity(project.getCity());
		projectEntity.setState(project.getState());
		projectEntity.setTotalPlotArea(project.getTotalPlotArea());
		projectEntity.setTotalUnit(project.getTotalUnit());
		projectEntity.setAssignTo(project.getAssignTo());
		projectEntity.setSourceName(project.getSourceName());
		projectEntity.setRequire(project.getRequire());
		projectEntity.setQuotation(project.getQuotation());
		projectEntity.setQuotationAmount(project.getQuotationAmount());
		projectEntity.setClose(project.getClose());
		projectEntity.setRemark(project.getRemark());
		projectEntity.setWorkInProgress(project.getWorkInProgress());
		projectEntity.setSideVisitedBy(project.getSideVisitedBy());
		projectEntity.setUploadDate(project.getUploadDate());
		projectRepository.saveAndFlush(projectEntity);
		return "Project updated successfully.";
		
	}
	
	public String deleteProject(Long id) {
		Optional<Project> optionalProject = projectRepository.findById(id);
		if(optionalProject != null) {
			projectRepository.deleteById(id);
		}		
		return "Project Deleted SuccessFully"+ optionalProject.get().getName()+", "+  optionalProject.get().getId();
	}  

	public Long getProjectCount() {
		return projectRepository.count();
	}

}
