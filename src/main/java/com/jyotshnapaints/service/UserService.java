package com.jyotshnapaints.service;


import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jyotshnapaints.dto.UserDTO;
import com.jyotshnapaints.entity.User;
import com.jyotshnapaints.repository.IUserRepository;

@Service
public class UserService {
	
	
	@Autowired
	private IUserRepository userRepository;

	public List<User > getUsers() {
		return userRepository.findAll();
	}

	public Optional<User> getUsertById(Long id) {
		return userRepository.findById(id);
	}

	public String insertUser(UserDTO user) {
		userRepository.saveAndFlush(UserDTO.prepareUserEntity(user));
		return "Project Added successfully";
	}

	public String deleteUser(Long id) {
		Optional<User> optionalUser = userRepository.findById(id);
		if(optionalUser != null) {
			userRepository.deleteById(id);
		}		
		return "user Deleted SuccessFully"+ optionalUser.get().getName()+", "+  optionalUser.get().getId();
	}
	

	public String updateUser(UserDTO user, Long id) {
		Optional<User> optionalUser = userRepository.findById(id);
		  User userEntity = optionalUser.get();
		  
		  userEntity.setName(user.getName());
		  userEntity.setEmailid(user.getEmailid());
		  userEntity.setPhonenumber(user.getPhonenumber());
		  userEntity.setPassword(user.getPassword());
		  userEntity.setRole(user.getRole());
		  userRepository.saveAndFlush(userEntity);
		  return "User updated successfully.";
	}
}
