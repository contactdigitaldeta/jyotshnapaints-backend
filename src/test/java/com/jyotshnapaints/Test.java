package com.jyotshnapaints;

public class Test extends Thread {
	
	public static int amount = 0;

	  public static void main(String[] args) {
		  Test thread1 = new Test();
		  Test thread2 = new Test();
	    
	    Mail m1 = new Mail();
	    Message m2 = new Message();
	    
//	    for(int i=0; i<10; i++) {
//	    	
//	        m1.start();
//	        m2.start();
//	    }
	    
	    thread1.start();
	    thread2.start();
	    System.out.println("1st "+amount);
	    //amount++;
	    System.out.println("2nd "+amount);
	  }

	  public void run() {
	  	System.out.println("inside run "+amount);
	    amount++;
	  }

}

class Mail extends Thread {

	public void run() {
    System.out.println("Mail running");
  }

}

class Message extends Thread {

	public void run() {
    System.out.println("Message running");
  }

}

